#!/usr/bin/python
import os
import re
import requests
import subprocess
import sys
from bs4 import BeautifulSoup
from urllib.parse import urljoin


class DownloadURL(object):
    """docstring for DownloadURL"""

    def __init__(self, site, url, data, nodown):
        self.basename = site
        self.url = url
        self.data = data
        self.nodown = nodown


class Site(object):
    def __init__(self, site_re):
        self.site_re = site_re


s = requests.Session()

uahead = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.183 Safari/537.36 Vivaldi/1.96.1147.42"
}


def get_page(url, cookies=None, req_headers={}, verify=True):
    p = s.get(url, cookies=cookies, headers={**uahead, **req_headers}, verify=verify)
    return BeautifulSoup(p.text, "lxml")


def get_json(url, cookies=None, req_headers={}, verify=True):
    p = s.get(url, cookies=cookies, headers={**uahead, **req_headers}, verify=verify)
    return p.json()


def post_page(url, data=None, cookies=None, req_headers={}):
    p = s.post(url, data=data, cookies=cookies, headers={**uahead, **req_headers})
    return BeautifulSoup(p.text, "lxml")


def post_json(url, data=None, cookies=None, req_headers={}):
    p = s.post(url, data=data, cookies=cookies, headers={**uahead, **req_headers})
    return p.json()


def get_headers(
    url, data=None, params=None, cookies=None, req_headers={}, method="GET"
):
    p = s.request(
        method,
        url,
        data=data,
        params=params,
        cookies=cookies,
        headers={**uahead, **req_headers},
        allow_redirects=False,
    )
    return p.headers


def parse_form(form, basename):
    url = form["action"]
    if not url.startswith("http"):
        url = urljoin(basename, url)
    params = {i["name"]: i["value"] for i in form.findAll("input") if i["value"]}
    return url, params


if __name__ == "__main__":
    path = "sites/"
    sites = []
    sys.path.insert(0, path)
    for f in os.listdir(path):
        fname, ext = os.path.splitext(f)
        if ext == ".py" and fname != "__init__":
            mod = __import__(fname)
            try:
                site = getattr(mod, fname)
            except AttributeError as e:
                print(f"Site error {fname}: {e}")
                continue
            sites.append(site())

    sys.path.pop(0)

    print("%d sites loaded." % len(sites))

    data = False
    nodown = False
    downloadurls = []
    pageurls = sys.argv[1:]
    for pageurl in pageurls:
        # if re.compile(site_re)
        for entry in sites:
            if re.compile(entry.site_re).search(pageurl):
                site = entry
                break

        downloadurls.append(site.parse(pageurl))

    # print('\n'.join([i.url for i in downloadurls]))
    for url in downloadurls:
        print(url.url, url.data)
        if url.nodown:
            print(url.url)
        else:
            cmd = [
                "wget2",
                "--user-agent",
                uahead["User-Agent"],
                "--no-check-certificate",
                "--content-disposition",
                "--referer",
                url.basename,
                "--progress",
                "bar",
                "--trust-server-names",
                url.url,
            ]
            if url.data:
                cmd += ["--post-data", url.data]
            subprocess.run(cmd)
