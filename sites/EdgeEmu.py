from __main__ import Site, DownloadURL
from re import findall


class EdgeEmu(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?edgeemu\.net")

    def parse(self, pageurl):
        # basename = 'https://edgeemu.net'
        basename = pageurl
        (id_,) = findall(r"/details-(\d+)\.htm", pageurl)
        return DownloadURL(
            basename, f"https://edgeemu.net/down.php?id={id_}", None, False
        )
