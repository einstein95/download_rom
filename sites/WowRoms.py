from __main__ import Site, DownloadURL, get_page, get_json
from re import findall, sub
from time import time
from hashlib import md5
from urllib.parse import urlencode


class WowRoms(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?wowroms\.com")

    def parse(self, pageurl):
        basename = "https://wowroms.com"
        if "/download-" in pageurl:
            referer = sub(r"/download-", "", pageurl)
        else:
            referer = pageurl
            pageurl = sub(r"(.+/roms/[^/]+/)(.+)", r"\1download-\2", pageurl)

        p = get_page(pageurl, req_headers={"Referer": referer})
        key = int(time() * 1000)
        token = md5(str(key).encode("ascii")).hexdigest()
        (downurl,) = findall(r'var\s+ajaxLinkUrl\s+=\s+"([^"]+)', str(p))
        j = get_json(f"{basename}{downurl}?k={key}&t={token}")
        emuid = p.find("input", attrs={"name": "emuid"})["value"]
        id_ = p.find("input", attrs={"name": "id"})["value"]
        file = p.find("input", attrs={"name": "file"})["value"]
        return DownloadURL(
            basename,
            j["link"],
            urlencode({"emuid": emuid, "id": id_, "file": file}),
            False,
        )
