from __main__ import Site, DownloadURL, get_page, parse_form, get_headers
from urllib.parse import urlencode


class PlanetEmu(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?planetemu\.net/")

    def parse(self, pageurl):
        basename = "https://www.planetemu.net"
        tmpurl = f"{basename}/php/roms/download.php"
        p = get_page(pageurl)
        url, params = parse_form(p.find("form", class_="downloadForm"), basename)
        location = get_headers(
            tmpurl,
            method="POST",
            data=urlencode(params),
            req_headers={"Content-Type": "application/x-www-form-urlencoded"},
        )["Location"]
        url = tmpurl + location
        return DownloadURL(basename, url, None, False)
