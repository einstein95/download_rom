from __main__ import Site, DownloadURL, get_page
from re import search, findall


class Gamulator(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?gamulator\.com")

    def parse(self, pageurl):
        basename = "https://www.gamulator.com"
        if pageurl.endswith("/download/fast"):
            pageurl = pageurl[: pageurl.index("/fast")]
        p = get_page(pageurl)
        if search(r"/download($|/)", pageurl):
            (url,) = findall(r'window.location.href="([^"]+)', str(p))
        else:
            p = get_page(basename + p.findAll("a", {"id": "btnDownload"})[1]["href"])
            (url,) = findall(r'window.location.href="([^"]+)', str(p))
        return DownloadURL(basename, url, None, False)
