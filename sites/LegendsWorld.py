from __main__ import Site, DownloadURL, get_page
from re import findall
from urllib.parse import unquote


class LegendsWorld(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?legendsworld\.net/")

    def parse(self, pageurl):
        basename = "https://www.legendsworld.net"
        p = get_page(pageurl)
        script = p.findAll("script")[0].contents[0]
        script = unquote(findall(r'eval\(unescape\("(.+?)\\"\)\)', script)[0])
        for result in findall(r'remote_http\\",\\"([^"\n]+)', script):
            return DownloadURL(basename, result, None, True)
