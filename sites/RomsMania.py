from __main__ import Site, DownloadURL, get_page


class RomsMania(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?romsmania\.cc")

    def parse(self, pageurl):
        basename = "https://romsmania.cc"
        p = get_page(pageurl)
        dlp = p.findAll("a", id="download_link")[0]["href"]
        p = get_page(dlp)
        url = p.findAll("a", class_="wait__link")[0]["href"]
        return DownloadURL(basename, url, None, False)
